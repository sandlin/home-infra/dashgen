import configparser
import logging
import peewee
from os import path
import os
from dashgen.prometheus import Prometheus
from dashgen.exporter import Exporter
__version__ = '0.1.0'

SQLITE_DB_FILE = "./dashgen.db"

class DashGen:
    def __init__(self, **kwargs):
        """
        Primary entry point for Grafana DashGen
        """
        print("_"*30)
        print(os.getcwd())
        print("_"*30)
        self._load_config()
        logging.basicConfig(level=self.log_level)
        self.log = logging.getLogger(__name__)
        self.log.debug("DashGen.init()")
        self.db = peewee.SqliteDatabase(SQLITE_DB_FILE)
        self.prometheus = Prometheus(self.prometheus_url, logging)
        self.exporter = Exporter(self.db, logging)




    def _load_config(self, config_file: str = None) -> None:
        """
        Load SalesForce auth config from file.
        :return:
        """
        self.config = {}
        if config_file is None:
            config_file = "config.toml"#path.join(path.expanduser("~"), ".salesforce")
        cp = configparser.ConfigParser()
        try:
            cp.read(config_file)
            print(cp)
            if "grafana" not in cp.sections():
                raise Exception("Your config.toml is missing the grafana section.")
            if "prometheus" not in cp.sections():
                raise Exception("Your config.toml is missing the prometheus section.")

            self.log_level = cp.get("logging", "level")
            self.grafana_url = cp.get("grafana", "url")
            self.prometheus_url = cp.get("prometheus", "url")
        except configparser.NoSectionError as e:
            raise Exception("Unable to find salesforce in your config file.")
