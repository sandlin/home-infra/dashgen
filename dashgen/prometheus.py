import requests
import json

class Prometheus:

    def __init__(self, url, logging):
        self.url =  url
        self.log = logging.getLogger(__name__)
        self.log.debug("Prometheus.init()")
        self.query_url = self.url + '/api/v1/query'


    def query(self, query_string):
        try:
            response = requests.get(self.query_url,
                                    params={'query': query_string})
            query_results = json.loads(response.text)
            return(query_results)
        except Exception as e:
            raise(e)

# with open(filename, 'r') as stream:
#     content = yaml.load(stream)
#     for k,v in content.items():
#         if "URLLogin.txt" in v:
#             print k, v
#
    def metric_list(self, **kwargs):
        filter = ""
        if "job" in kwargs and kwargs['job'] is not None:
            filter += 'job="{}"'.format(kwargs.pop('job'))
            if "instance" in kwargs and kwargs['instance'] is not None:
                filter += ", "
        if "instance" in kwargs and kwargs['instance'] is not None:
            filter += 'instance="{}"'.format(kwargs.pop('instance'))

        fs = '__name__!~""'
        if filter != "":
            fs = fs + ', ' + filter

        qs = 'group by(__name__)({{{fs}}})'.format(fs=fs)
        self.log.debug(qs)
        qr = self.query(qs)
        return qr['data']['result']


    def instance_list(self):
        qs = 'group by(instance)({__name__!=""})'
        self.log.debug(qs)
        qr = self.query(qs)
        return qr['data']['result']


    def job_list(self):
        qs = 'group by(job)({__name__!=""})'
        self.log.debug(qs)
        qr = self.query(qs)
        return qr['data']['result']
