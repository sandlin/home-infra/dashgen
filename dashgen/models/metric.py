class Metric:

    def __init__(self):
        self.name = ""
        self.desc = ""
        self.type = None
        self.value = None



    def to_dict(self) -> dict:
        """
        Dumps this object to dict

        :return: object as dict
        :rtype: dict
        """
        return {k: v for k, v in self.__dict__.items() if not k.startswith("__")}
