# https://github.com/getredash/redash/blob/master/redash/cli/__init__.py
"""
CLI tool to manage SalesForce in GitLab
"""
import click
from dashgen import DashGen
from dashgen.cli.prometheus import cli as prom_cli
from dashgen.cli.exporter import cli as exporter_cli
#
# from glsf.cli.opportunity import cli as opportunity_cli
# from glsf.cli.sa import cli as sa_cli
# from glsf.cli.sal import cli as sal_cli
# from glsf.cli.user import cli as user_cli
# from glsf.galactus import Galactus


@click.group("cli")
@click.option("--verbosity", "-v", count=True, default=0)
@click.version_option()
@click.pass_context
def cli(ctx, **kwargs):
    """Python DashGen CLI EntryPoint"""
    if kwargs.get("verbosity") > 2:
        click.echo(kwargs)
    ctx.obj = DashGen(**kwargs)
    ctx.ensure_object(DashGen)

cli.add_command(prom_cli, "promql")
cli.add_command(exporter_cli, "exporter")