import click


@click.group("View Data from Prometheus")
@click.pass_context
def cli(ctx):
    """User entrypoint group"""
    pass


@cli.command()
#@click.option("--instances", "-i", is_flag=True, default=False, help="List Instances")
@click.pass_obj
def instances(obj):
    try:
        il = obj.prometheus.instance_list()
        print("-"*10)
        print("INSTANCES")
        for i in il:
            if 'instance' in i['metric']:
                print("- " + i['metric']['instance'])
        print("-"*10)
    except Exception as e:
        raise e


@cli.command()
#@click.option("--instances", "-i", is_flag=True, default=False, help="List Instances")
@click.pass_obj
def jobs(obj):
    try:
        il = obj.prometheus.job_list()
        print("-"*10)
        print("JOBS")
        for i in il:
            if 'job' in i['metric']:
                print("- " + i['metric']['job'])
        print("-"*10)
    except Exception as e:
        raise e


@cli.command()
@click.option("--job", "-j", help="Job Name")
@click.option("--instance", "-i", help="Instance Name")
@click.pass_obj
def metrics(obj, job, instance):
    try:
        il = obj.prometheus.metric_list(job=job, instance=instance)
        print("-"*10)
        print("METRICS")
        if job is not None: print("job = {}".format(job))
        if instance is not None: print("instance = {}".format(instance))
        print("-"*10)
        for i in il:
            if '__name__' in i['metric']:
                print("- " + i['metric']['__name__'])
        print("-"*10)
    except Exception as e:
        raise e