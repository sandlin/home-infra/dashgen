import click

@click.group("View Data from Exporters")
@click.pass_context
def cli(ctx):
    """User entrypoint group"""
    pass


@cli.command()
#@click.option("--instances", "-i", is_flag=True, default=False, help="List Instances")
@click.option("--host", required=True, type=click.Choice(['dell', 'imac', 'qnap', 'pfsense'], case_sensitive=False), help="The hostname you'd like to query")
@click.pass_obj
def metrics(obj, host):
    try:
        metrics = obj.exporter.pull_and_process_metrics(host)
        for m in metrics:
            print(m.to_dict())
        #print("-"*10)
        #print("METRICS for {}".format(host))
        #for i in il:

        #print("-"*10)
    except Exception as e:
        raise e
