import requests
import json
import re
from models.metric import Metric

class Exporter:

    def __init__(self, db_conn, logging):
        self.db_conn = db_conn
        self.log = logging.getLogger(__name__)
        self.host_list = ['dell', 'imac', 'qnap', 'pfsense']
        self.host_list_map = map(re.compile, self.host_list)

    def pull_and_process_metrics(self, host):
        il = self.pull_metrics(host)
        return self.process_metrics(host, il)

    def pull_metrics(self, host):
        '''
        #
        # http://dell.h8n.lan:9100/metrics
        # http://pfsense.h8n.lan:9100/metrics
        # http://imac.h8n.lan:9100/metrics
        #
        # http://snmp_exporter.h8n.lan:9100/metrics
        :param target:
        :return:
        '''

        if any(known_host.match(host) for known_host in self.host_list_map):
            req_url = "http://{}.h8n.lan:9100/metrics".format(host)
        else:
            return None
        try:
            response = requests.get(req_url)
            return response.text
        except Exception as e:
            raise(e)


    def process_metrics(self, host, metrics):
        ht = re.compile('^#\s+HELP\s+([\w_]+)\s(.*)\n#\s+TYPE\s+[\w_]+\s(.*)', re.MULTILINE)
        matches = ht.findall(metrics)
        metrics = []
        for g in matches:
            m = Metric()
            m.name = g[0]
            m.desc = g[1]
            m.type = g[2]
            metrics.append(m)
        return metrics

        #print(matches.group(0))
        # hl = re.compile('^#\s+HELP\s+([\w_]+)\s(.*)')
        # tl = re.compile('^#\s+TYPE\s+[\w_]+\s(.*)')
        # for line in metrics.splitlines():
        #     hm = hl.search(line)
        #     if hm:
        #         m = Metric()
        #         m.name = hm.group(1)
        #         m.desc = hm.group(2)
        #         m.type = hm.group(3)
        #         print(hm.group(1))
            #if tm:
            #    print(line)
            #print(line)


