from unittest.mock import patch
from unittest import TestCase
import logging
import json

from dashgen.exporter import Exporter

class TestExporter(TestCase):

    def __init__(self, *args, **kwargs):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)
        self.exporter = Exporter('http://snmp.expor.ter', logging)
        self.rs = {"status": "success", "data": {"resultType": "vector", "result": [{"metric": {"instance": "http://www.google.com/"}, "value": [1652891116.155, "1"]}, {"metric": {"instance": "https://sandlininc.com/"}, "value": [1652891116.155, "1"]}]}}
        super().__init__(*args, **kwargs)

    # This method will be used by the mock to replace requests.get
    def mocked_requests_get(*args, **kwargs):
        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code
                self.text = json.dumps(json_data)

        mock_query_results = {"status": "success","data": {"resultType": "vector","result": [{"metric": {"instance": "http://www.google.com/"},"value": [1652891116.155, "1"]},{"metric": {"instance": "https://sandlininc.com/"},"value": [1652891116.155, "1"]}]}}
        return MockResponse(mock_query_results, 200)

    @patch('requests.get', side_effect=mocked_requests_get)
    def test_query(self, mg):
        self.assertEqual(self.prom.query("foo"), self.rs)

    @patch('requests.get', side_effect=mocked_requests_get)
    def test_metric_list(self, mg):
        self.assertEqual(self.prom.metric_list(), self.rs['data']['result'])
        self.assertEqual(self.prom.metric_list(job="foo"), self.rs['data']['result'])
        self.assertEqual(self.prom.metric_list(instance="bar"), self.rs['data']['result'])
        self.assertEqual(self.prom.metric_list(job="foo", instance="bar"), self.rs['data']['result'])

    @patch('requests.get', side_effect=mocked_requests_get)
    def test_instance_list(self, mg):
        self.assertEqual(self.prom.instance_list(), self.rs['data']['result'])

    @patch('requests.get', side_effect=mocked_requests_get)
    def test_job_list(self, mg):
        # print("-"*20)
        # print(self.prom.job_list())
        # print("-"*20)
        # print(self.rs['data']['result'])
        # print("-"*20)
        self.assertEqual(self.prom.job_list(), self.rs['data']['result'])
